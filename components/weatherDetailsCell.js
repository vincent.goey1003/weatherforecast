import React, {Component} from 'react';
import {View, Image, Text, StyleSheet, TouchableOpacity} from 'react-native';
import UtilsHelper from './utils';

export const WeatherDetailsCell = props => {
  const {data} = props;
  return (
    <View style={styles.cellContainer}>
      <View style={styles.imageContainer}>
        <Image
          style={styles.image}
          source={{
            uri: `http://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png`,
          }}
        />
      </View>
      <View style={styles.labelContainer}>
        <Text style={styles.font12}>
          {UtilsHelper.returnTime(data.dt, true)}
        </Text>
        <Text style={styles.font12}>{`Temparature: ${UtilsHelper.returnCelcius(
          data.main.temp,
        )}`}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  cellContainer: {
    height: 80,
    marginHorizontal: 20,
    marginTop: 5,
    backgroundColor: 'rgba(255,255,255,0.4)',
    flexDirection: 'row',
  },
  image: {
    height: 80,
    width: 80,
  },
  imageContainer: {height: '100%', width: 80},
  font12: {fontSize: 12, color: 'black'},
  labelContainer: {flex: 1, justifyContent: 'center'},
});
