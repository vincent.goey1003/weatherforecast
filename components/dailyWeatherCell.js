import React, {Component} from 'react';
import {View, Image, Text, StyleSheet, TouchableOpacity} from 'react-native';
import UtilsHelper from './utils';

export const DailyWeatherCell = props => {
  const {data, onPressed} = props;
  return (
    <TouchableOpacity style={styles.cellContainer} onPress={onPressed}>
      <View style={styles.imageContainer}>
        <Image
          style={styles.image}
          source={{
            uri: `http://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png`,
          }}
        />
      </View>
      <View style={styles.labelContainer}>
        <Text>{`H:${UtilsHelper.returnCelcius(
          data.temp.max,
        )} L:${UtilsHelper.returnCelcius(data.temp.min)}`}</Text>
        <Text style={styles.font12}>{data.weather[0].main}</Text>
        <Text style={styles.font12}>{UtilsHelper.returnTime(data.dt)}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  cellContainer: {
    height: 80,
    marginHorizontal: 20,
    marginTop: 5,
    backgroundColor: 'rgba(255,255,255,0.4)',
    flexDirection: 'row',
  },
  image: {
    height: 80,
    width: 80,
  },
  imageContainer: {height: '100%', width: 80},
  font12: {fontSize: 12, color: 'black'},
  labelContainer: {flex: 1, justifyContent: 'center'},
});
