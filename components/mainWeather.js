import React, {Component} from 'react';
import {View, Image, Text, StyleSheet, Platform} from 'react-native';
import UtilsHelper from './utils';

export const TopWeatherContainer = props => {
  const {data} = props;

  return (
    <View style={styles.currentWeatherContainer}>
      <Image
        style={styles.image}
        source={{
          uri: data.weather
            ? `http://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png`
            : null,
        }}></Image>
      <Text style={styles.font28}>{`${
        data.name ? data.name : 'Loading Location'
      }`}</Text>
      <Text style={styles.font12}>{`${
        data.weather ? data.weather[0].main : 'Loading Weather'
      }`}</Text>
      <Text style={styles.font12}>{`${
        data.dt ? UtilsHelper.returnTime(data.dt) : ''
      }`}</Text>
      <Text style={styles.font40}>{`${
        data && data.main ? UtilsHelper.returnCelcius(data.main.temp) : ''
      }`}</Text>
      <Text style={styles.font12}>{`H:${
        data && data.main ? UtilsHelper.returnCelcius(data.main.temp_max) : ''
      } L:${
        data && data.main ? UtilsHelper.returnCelcius(data.main.temp_min) : ''
      }`}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  currentWeatherContainer: {
    height: 220,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
  },
  image: {
    height: 80,
    width: 80,
  },
  font28: {
    fontSize: 28,
    color: 'white',
    fontFamily: UtilsHelper.returnGrestalFontName(),
    fontWeight: 'normal',
  },
  font12: {fontSize: 12, color: 'white'},
  font40: {fontSize: 40, color: 'white'},
});
