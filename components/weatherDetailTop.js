import React, {Component} from 'react';
import {View, Image, Text, StyleSheet, TouchableOpacity} from 'react-native';
import UtilsHelper from './utils';

export const WeatherDetailsTopWeatherContainer = props => {
  const {data} = props;

  return (
    <View style={styles.currentWeatherContainer}>
      <Image
        style={styles.image}
        source={{
          uri: data.weather
            ? `http://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png`
            : null,
        }}></Image>
      <Text style={styles.font28}>{`${
        data.dt ? UtilsHelper.returnTime(data.dt) : ''
      }`}</Text>
      <Text style={styles.font12}>{`${
        data.weather ? data.weather[0].main : 'Loading Weather'
      }`}</Text>
      <Text style={styles.font12}>{`H:${
        data && data.temp ? UtilsHelper.returnCelcius(data.temp.max) : ''
      } L:${
        data && data.temp ? UtilsHelper.returnCelcius(data.temp.min) : ''
      }`}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  currentWeatherContainer: {
    height: 180,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
  image: {
    height: 80,
    width: 80,
  },
  font28: {fontSize: 28, color: 'white'},
  font12: {fontSize: 12, color: 'white'},
  font40: {fontSize: 40, color: 'white'},
});
