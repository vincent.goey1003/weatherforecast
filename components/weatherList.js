import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  AppState,
  Text,
  Platform,
  Alert,
} from 'react-native';
import Geolocation from '@react-native-community/geolocation';
import {useNavigation} from '@react-navigation/native';
import {TopWeatherContainer} from './mainWeather';
import {DailyWeatherCell} from './dailyWeatherCell';
import {useDispatch, useSelector} from 'react-redux';
import {getCurrentWeather} from './store/action/weatherActions';
import UtilsHelper from './utils';

console.disableYellowBox = true;

export default function RetrieveWeather() {
  const dispatch = useDispatch();

  const appState = useRef(AppState.currentState);
  const [appStateVisible, setAppStateVisible] = useState(appState.current);
  const navigation = useNavigation();
  const [dailyWeather, setDailyWeather] = useState([]);
  const [currentWeather, setCurrentWeather] = useState({});
  const [every3HoursForecast, setEvery3HoursForecast] = useState({});
  const [getLocationStatus, setGetLocationStatus] = useState(true);
  const [err, setErr] = useState('');

  const isRedux = false;
  const {data, error} = useSelector(state => state.weather);

  useEffect(() => {
    getLocation();
    AppState.addEventListener('change', _handleAppStateChange);

    return () => {
      AppState.removeEventListener('change', _handleAppStateChange);
    };
  }, []);

  const _handleAppStateChange = nextAppState => {
    if (
      appState.current.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      getLocation();
    }

    appState.current = nextAppState;
    setAppStateVisible(appState.current);
  };

  function getLocation() {
    //geo location bug
    let geoOptions =
      Platform.OS === 'android'
        ? {}
        : {enableHighAccuracy: true, timeout: 20000, maximumAge: 10000};
    Geolocation.getCurrentPosition(geoSuccess, geoFailure, geoOptions);
  }

  function geoSuccess(position) {
    setGetLocationStatus(true);

    if (isRedux) {
      dispatch(
        getCurrentWeather(position.coords.latitude, position.coords.longitude),
      );
    } else {
      callCurrentWeatherApi(
        position.coords.longitude,
        position.coords.latitude,
      );
    }

    callApi(position.coords.longitude, position.coords.latitude);
    call3HoursForecastWeatherApi(
      position.coords.longitude,
      position.coords.latitude,
    );
  }

  function geoFailure(err) {
    console.log(err.message);
    setGetLocationStatus(false);
    setErr(err.message);
  }

  function callApi(lon, lat) {
    fetch(
      `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&exclude=minutely&appid=${UtilsHelper.returnOpenWeatherAppkey()}`,
    )
      .then(res => res.json())
      .then(result => {
        var dailyWeatherList = result.daily;
        if (dailyWeatherList.length > 0) {
          dailyWeatherList.pop();
        }
        setDailyWeather(dailyWeatherList);
      })
      .catch(function (error) {
        setErr(error.message);
        throw error;
      });
  }

  function callCurrentWeatherApi(lon, lat) {
    fetch(
      `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}6&appid=${UtilsHelper.returnOpenWeatherAppkey()}`,
    )
      .then(res => res.json())
      .then(result => {
        setCurrentWeather(result);
      })
      .catch(function (error) {
        setErr(error.message);
        throw error;
      });
  }

  function call3HoursForecastWeatherApi(lon, lat) {
    fetch(
      `https://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lon}6&appid=${UtilsHelper.returnOpenWeatherAppkey()}`,
    )
      .then(res => res.json())
      .then(result => {
        setEvery3HoursForecast(result);
      })
      .catch(function (error) {
        console.log(
          'There has been a problem with your fetch operation: ' +
            error.message,
        );
        throw error;
      });
  }

  function returnLocationSuccessView() {
    return (
      <View style={styles.container}>
        <TopWeatherContainer data={isRedux && data ? data : currentWeather} />
        <FlatList
          keyExtractor={(item, index) => index.toString()}
          data={dailyWeather}
          renderItem={returnDailyWeatherList}
          style={styles.flat}
        />
      </View>
    );
  }

  function returnLocationFailView() {
    return (
      <View
        style={[
          styles.container,
          {alignItems: 'center', justifyContent: 'center'},
        ]}>
        <Text>Unable to Load Forecast Data</Text>
        <Text>Please Enable Location Serive or Turn on Network</Text>
        <Text>{`Error Message: ${isRedux && error ? error : err}`}</Text>
      </View>
    );
  }

  function returnDailyWeatherList(item) {
    const dailyWeather = item.item;
    return (
      <DailyWeatherCell
        data={dailyWeather}
        onPressed={() => weatherListOnclick(dailyWeather)}
      />
    );
  }

  function weatherListOnclick(dailyWeather) {
    //Condition need to be change if weather app is forecasting for everyday. This condition only applicable for 7 days (no date duplicated)
    const filterArray = every3HoursForecast.list.filter(d => {
      return (
        new Date(d.dt * 1000).getDate() ==
        new Date(dailyWeather.dt * 1000).getDate()
      );
    });

    if (filterArray.length > 0) {
      navigation.navigate('WeatherDetails', {
        weatherData: filterArray,
        dailyWeather: dailyWeather,
      });
    } else {
      Alert.alert('No Data from Api!', [{text: 'OK'}]);
    }
  }

  return getLocationStatus
    ? returnLocationSuccessView()
    : returnLocationFailView();
}

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: '#00d2ff'},
  flat: {flex: 1, marginBottom: 20},
});
