import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import WeatherDetails from '../weatherDetails';
import WeatherList from '../weatherList';

const Stack = createStackNavigator();

const MyStack = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="WeatherList">
        <Stack.Screen
          name="WeatherDetails"
          component={WeatherDetails}
          options={{title: 'Weather Details', headerBackTitleVisible: false}}
        />
        <Stack.Screen
          name="WeatherList"
          component={WeatherList}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default MyStack;
