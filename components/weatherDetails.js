import React from 'react';
import {StyleSheet, View, FlatList} from 'react-native';
import {WeatherDetailsTopWeatherContainer} from './weatherDetailTop';
import {WeatherDetailsCell} from './weatherDetailsCell';

const WeatherDeatils = _navigation => {
  const weatherDetail = _navigation.route.params.weatherData;
  const dailyWeather = _navigation.route.params.dailyWeather;

  return (
    <View style={styles.container}>
      <WeatherDetailsTopWeatherContainer data={dailyWeather} />
      <FlatList
        keyExtractor={(item, index) => index.toString()}
        data={weatherDetail}
        renderItem={returnDailyWeatherList}
        style={styles.flat}
      />
    </View>
  );

  function returnDailyWeatherList(item) {
    const forecastFor3Hours = item.item;
    return <WeatherDetailsCell data={forecastFor3Hours} />;
  }
};

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: '#00d2ff'},
  flat: {flex: 1, marginBottom: 50},
});

export default WeatherDeatils;
