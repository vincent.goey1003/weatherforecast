const appKey = '436cbdafa429bc08e08f206bf049bdee';

export const getCurrentWeather = (
  lat,
  lon,
  onSuccess = () => {},
  onError = () => {},
) => {
  return async dispatch => {
    try {
      const res = await fetch(
        `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}6&appid=${appKey}`,
      );

      if (!res.ok) {
        const resData = await res.json();
        throw new Error(resData.message);
      }

      const resData = await res.json();
      dispatch({
        type: 'GET_CURRENT_WEATHER',
        payload: resData,
      });
      onSuccess();
    } catch (err) {
      dispatch(setError(err.message));
      onError();
    }
  };
};

const setError = err => {
  return {
    type: 'ERROR',
    payload: err,
  };
};
