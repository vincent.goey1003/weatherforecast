const initialState = {
  data: null,
  error: '',
};

export default (state = initialState, action) => {
  console.log('Reducer: ', action.type);
  switch (action.type) {
    case 'GET_CURRENT_WEATHER':
      return {
        data: action.payload,
        error: '',
      };
    case 'ERROR':
      return {
        ...state,
        error: action.payload,
      };
    default:
      return state;
  }
};
