import {Platform} from 'react-native';

let Helper = {};

Helper.returnCelcius = temp => {
  //Celcius convert format
  return `${Math.round(temp - 273.15)}°C `;
};

Helper.returnTime = (timestamp, isHour = false) => {
  let unix_timeStamp = timestamp;
  let date = new Date(unix_timeStamp * 1000);
  let hours =
    `${date.getHours()}`.length > 1 ? date.getHours() : '0' + date.getHours();
  let dayAlpha = date.getDay();
  let day = date.getDate();
  let month = date.getUTCMonth();
  let days = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
  ];

  const monthNames = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

  return isHour
    ? `${hours}:00`
    : `${days[dayAlpha]} ${monthNames[month]} ${day}`;
};

Helper.returnGrestalFontName = () => {
  return Platform.OS === 'android'
    ? 'GrestalScriptDemoRegular-dBYX'
    : 'GrestalScriptDEMO-Regular';
};

Helper.returnOpenWeatherAppkey = () => {
  return '436cbdafa429bc08e08f206bf049bdee';
};

export default Helper;
