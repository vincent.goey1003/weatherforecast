Weather Forecast Assessment

Api: 

1.weather -> for current weather data <br />
2.onecall -> for next 7 days forecast data <br />
3.forecast -> for next 5 days forecast every 3 hours data <br />

Implemented Function:

Redux -> have to turn const isRedux to true in weatherList.js <br />
AppState -> monitor app back to foreground and refresh weather data <br />
Geo Location -> Track device location to get weather data <br />
Hooks -> manage state and ui rendering <br />

Remarks* -> this project only build with 2 screens, therefore doesn't rely on redux function that redux is built for large complexity, for small project, hook and mobx is good enough to handle state. Redux function just a sample of implementation from requirement.

Limitation:

For location have to preset in simulator/emulator/real device only can proceed with weather data

Future Implementation:

Add react-native-permission to check wheather user allow location permission. <br />
Allow to key in city name to check others city weather forecast <br />
Call weather data every 3 hours interval <br />

Built Tools Version: <br />
Visual Studio Code <br />
Xcode 12.5 <br />
Android Studio 4.1 <br />

Screenshot is provided in screenshot folder.

Please run ./projectInit in terminal or double click projectInit file before launch the app.