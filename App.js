import React from 'react';
import 'react-native-gesture-handler';
import ScreenStack from './components/navigationService/router';
import {Provider} from 'react-redux';
import store from './components/store';

export default function App() {
  return (
    <Provider store={store}>
      <ScreenStack />
    </Provider>
  );
}
